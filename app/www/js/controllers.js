angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, Chats, $state) {

  $scope.add = function(){
    Chats.add($scope.text).then(function(){
      $scope.text = '';
      $state.go('tab.chats');
    });
  }

})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  function getData(){
    Chats.all().then(function(tasks){
      $scope.tasks = tasks;
    });
  }

  $scope.remove = function(chat) {
    Chats.remove(chat).then(getData);
  };

  getData();
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
