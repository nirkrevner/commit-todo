angular.module('starter.services', [])

.service('cordovaSrvc', function($window, $q, $ionicPlatform){

  var that = this;

  this.is = function(){
    return !!$window.cordova;
  }

  this.getSimSerial = function(){
    var defer = $q.defer();

    if (!that.is()){
      defer.resolve('dev');
    } else if (that._simSerial){
      defer.resolve(that._simSerial);
    } else {
      $ionicPlatform.ready(function(){
        window.plugins.sim.getSimInfo(function(res){
          that._simSerial = res.simSerialNumber;
          defer.resolve(that._simSerial);
        }, defer.reject);
      });
    }

    this.apiPrefix = function(){
      return that.is() ? 'https://test505.herokuapp.com' : '';
    }

    return defer.promise;
  }

})

.factory('Chats', function($q, cordovaSrvc, $http) {

  return {
    all: function() {
      var defer = $q.defer();
      cordovaSrvc.getSimSerial().then(function(serial){
        $http.get(cordovaSrvc.apiPrefix() + '/api/uid/'+serial+'/tasks/get').then(function(res){
          defer.resolve(res.data);
        });
      });
      return defer.promise;
    },
    remove: function(chat) {
      var defer = $q.defer();
      cordovaSrvc.getSimSerial().then(function(serial){
        $http.post(cordovaSrvc.apiPrefix() + '/api/uid/'+serial+'/tasks/delete', chat)
            .then(defer.resolve, defer.reject);
      });

      return defer.promise;
    }, 
    add: function(text){
      var defer = $q.defer();

      cordovaSrvc.getSimSerial().then(function(serial){
        $http.post(cordovaSrvc.apiPrefix() + '/api/uid/'+serial+'/tasks/create', {name: text})
          .then(defer.resolve, defer.reject);
      });
      
      return defer.promise;
    }
  };
});
