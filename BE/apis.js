var express = require('express');
var router = express.Router();

var taskModel = require('./taskModel.js')

router.get('/api/uid/:uid/tasks/get', function(req, res){
	taskModel.find({userId: req.params.uid}).then(function(tasks){
		res.send(tasks);
	}, function(err){
		res.status(400).send({err: err});
	});
});

router.post('/api/uid/:uid/tasks/create', function(req, res){
	var newTask = new taskModel({
		userId: req.params.uid,
		name: req.body.name,
		createDate: new Date().getTime()
	});

	newTask.save().then(function(){
		res.send();
	}, function(err){
		res.status(400).send({err: err});
	});
});

router.post('/api/uid/:uid/tasks/delete', function(req, res){

	taskModel.find(req.body).remove().then(function(){
		res.send();
	}, function(err){
		res.status(400).send({err: err});
	});
});

router.get('/getApp', function(req, res){
	var file = __dirname + '/apk/android-debug.apk';
	res.download(file);
});

module.exports = router;