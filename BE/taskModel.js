var mongoose = require('mongoose');
mongoose.connect('mongodb://todoapp:123456@ds011382.mlab.com:11382/heroku_cx39kfwg');

console.log('Mongodb connection [OK]');

var taskModel = mongoose.model('Tasks', {
	name: String,
	createDate: String,
	userId: String
});

module.exports = taskModel;
